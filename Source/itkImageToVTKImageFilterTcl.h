#ifndef _ITK_IMAGETOVTKIMAGEFILTER_TCL_H
#define _ITK_IMAGETOVTKIMAGEFILTER_TCL_H

#include <tcl.h>

int ITKTCL_ImageToVTKImageFilter_Init(Tcl_Interp *interp);

#endif
