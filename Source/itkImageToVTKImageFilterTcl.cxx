#include "itkImageToVTKImageFilterTcl.h"

int ITKTCL_ImageToVTKImageFilter_Float_Init(Tcl_Interp *interp);
int ITKTCL_ImageToVTKImageFilter_Vector_Init(Tcl_Interp *interp);

int ITKTCL_ImageToVTKImageFilter_Init(Tcl_Interp *interp)
{
  int init1 = ITKTCL_ImageToVTKImageFilter_Float_Init(interp);
  int init2 = ITKTCL_ImageToVTKImageFilter_Vector_Init(interp);

  return init1 && init2;
}
