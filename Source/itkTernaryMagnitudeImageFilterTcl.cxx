#include "itkTernaryMagnitudeImageFilterTcl.h"
#include "itkSubCommandsTcl.h"
#include "itkTernaryMagnitudeImageFilter.h"

#define ITK_FILTER_NAME TernaryMagnitudeImageFilter
#define TCL_COMMAND_NAME TernaryMagnitudeFilter
#define TCL_COMMAND_PROC ITKTCL_PASTE(TCL_COMMAND_NAME, Cmd)
#define TCL_COMMAND_CREATE_PROC ITKTCL_PASTE(Create,TCL_COMMAND_PROC)

typedef itk::ITK_FILTER_NAME <Scalar3DImageType,Scalar3DImageType,
                              Scalar3DImageType,Scalar3DImageType> FilterType;


int TCL_COMMAND_PROC(ClientData clientData,
                     Tcl_Interp *interp,
                     int objc,
                     Tcl_Obj *const objv[])
{
  int tclStatus;
  
  FilterType::Pointer theFilter =
    ITKTCL_GetObjectFromClientData< FilterType >( clientData );

  if (!theFilter)
    {
    Tcl_AppendResult( interp, "command ",
                      Tcl_GetString(objv[0]),
                      " does not references a valid "
                      ITKTCL_TO_STRING(TCL_COMMAND_NAME),
                      NULL );
    return TCL_ERROR;
    }
  if ( objc < 2 )
    {
    const char* cmd = Tcl_GetString(objv[0]);
    Tcl_AppendResult( interp, ITKTCL_TO_STRING(TCL_COMMAND_NAME) " ERROR --\n",
                      "wrong # of arguments, must be:\n",
                      "    ", cmd, " AddObserver Event Script\n",
                      "    ", cmd, " GetProgress\n",
                      "    ", cmd, " SetInput1 ScalarImage3D\n",
                      "    ", cmd, " SetInput2 ScalarImage3D\n",
                      "    ", cmd, " SetInput3 ScalarImage3D\n",
		      "    ", cmd, " GetOutput\n",
                      "    ", cmd, " Update\n",
                      NULL );
    return TCL_ERROR;
  }
  // AddObserver
  else if ((tclStatus = ITKTCL_tryAddObserver(theFilter,
                                              ITKTCL_TO_STRING(TCL_COMMAND_NAME),
                                              interp, objc, objv)) != TCL_CONTINUE)
    {
    return tclStatus;
    }
  // GetProgress
  else if ((tclStatus = ITKTCL_tryGetProgress(theFilter,
                                              ITKTCL_TO_STRING(TCL_COMMAND_NAME),
                                              interp, objc, objv)) != TCL_CONTINUE)
    {
    return tclStatus;
    }
  // SetInput1
  else if ((tclStatus =
            ITKTCL_trySetInput1<FilterType>(theFilter,
                                            ITKTCL_TO_STRING(TCL_COMMAND_NAME),
                                            "Scalar3DImageType",
                                            interp, objc, objv)) != TCL_CONTINUE)
    {
    return tclStatus;
    }
  // SetInput2
  else if ((tclStatus =
            ITKTCL_trySetInput2<FilterType>(theFilter,
                                            ITKTCL_TO_STRING(TCL_COMMAND_NAME),
                                            "Scalar3DImageType",
                                            interp, objc, objv)) != TCL_CONTINUE)
    {
    return tclStatus;
    }
  // SetInput3
  else if ((tclStatus =
            ITKTCL_trySetInput3<FilterType>(theFilter,
                                            ITKTCL_TO_STRING(TCL_COMMAND_NAME),
                                            "Scalar3DImageType",
                                            interp, objc, objv)) != TCL_CONTINUE)
    {
    return tclStatus;
    }
  // GetOutput
  else if (!strcmp(Tcl_GetString(objv[1]), "GetOutput"))
    {
    return ITKTCL_GetOutput<Scalar3DImageType>(interp,
                                               theFilter->GetOutput());
    }
  // Update
  else if (!strcmp(Tcl_GetString(objv[1]), "Update"))
    {
    try
      {
      theFilter->Update();
      } 
    catch ( itk::ExceptionObject e )
      {
      Tcl_AppendResult( interp, e.GetDescription(), NULL );
      return TCL_ERROR;
      }
    return TCL_OK;
    }
  // wrong subcommand
  else 
    {
    const char* cmd = Tcl_GetString(objv[0]);
    Tcl_AppendResult( interp, ITKTCL_TO_STRING(TCL_COMMAND_NAME) " ERROR --\n",
                      "wrong subcommand '", Tcl_GetString(objv[1]),
                      "', must be:\n",
                      "    ", cmd, " AddObserver Event Script\n",
                      "    ", cmd, " GetProgress\n",
                      "    ", cmd, " SetInput1 ScalarImage3D\n",
                      "    ", cmd, " SetInput2 ScalarImage3D\n",
                      "    ", cmd, " SetInput3 ScalarImage3D\n",
		      "    ", cmd, " GetOutput\n",
                      "    ", cmd, " Update\n",
                      NULL);
    return TCL_ERROR;
    }
}

int TCL_COMMAND_CREATE_PROC( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[] )
{
  FilterType::Pointer theFilter = FilterType::New();
  
  return ITKTCL_CreateObjectCommand( interp,
                                     TCL_COMMAND_PROC,
                                     theFilter);
}

int ITKTCL_TernaryMagnitudeImageFilter_Init(Tcl_Interp *interp)
{
  Tcl_CreateObjCommand( interp, ITKTCL_TO_STRING(TCL_COMMAND_NAME),
                        TCL_COMMAND_CREATE_PROC,
                        NULL, NULL );
  return TCL_OK;
}
