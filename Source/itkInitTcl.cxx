#include "itkUtilsTcl.h"
#include "itkVersionTcl.h"
#include "itkImage3DReaderTcl.h"
#include "itkImageToVTKImageFilterTcl.h"
#include "itkComposeImageFilterTcl.h"
#include "itkShiftScaleImageFilterTcl.h"
#include "itkTernaryMagnitudeImageFilterTcl.h"
#include "vtkSampleParametricSplineTcl.h"

//#define ITKTCL_MAJOR_VERSION 2
//#define ITKTCL_MINOR_VERSION 0

extern "C" {
  int ITKTCL_EXPORT Itktcl_Init(Tcl_Interp *interp);
}

int Itktcl_Init(Tcl_Interp *interp)
{
  if (Tcl_InitStubs(interp, "8.5", 0) == NULL) 
    {
    return TCL_ERROR;
    }
  if (Tcl_PkgRequire(interp, "Tcl", "8.5", 0) == NULL) 
    {
    return TCL_ERROR;
    }
  
  ITKTCL_Utils_Init( interp );

  ITKTCL_Image3DReader_Init(interp);
  ITKTCL_ComposeImageFilter_Init(interp);
  ITKTCL_ShiftScaleImageFilter_Init(interp);
  ITKTCL_TernaryMagnitudeImageFilter_Init(interp);

  ITKTCL_ImageToVTKImageFilter_Init(interp);
  ITKTCL_SampleParametricSplineTcl_Init(interp);

  char pkgName[]="itktcl";
  //char pkgVers[]=ITKTCL_TO_STRING(ITKTCL_MAJOR_VERSION) "." ITKTCL_TO_STRING(ITKTCL_MINOR_VERSION);
  Tcl_PkgProvide(interp, pkgName, ITKTCL_VERSION_STRING);
  return TCL_OK;
}

