#ifndef _ITK_SHIFTSCALEIMAGEFILTER_TCL_H
#define _ITK_SHIFTSCALEIMAGEFILTER_TCL_H

#include <tcl.h>

int ITKTCL_ShiftScaleImageFilter_Init( Tcl_Interp *interp );
  
#endif
