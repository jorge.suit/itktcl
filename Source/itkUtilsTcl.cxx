#include "itkUtilsTcl.h"
#include <string.h>

int ITKTCL_ObjectCommand( ClientData     clientData,
                          Tcl_Interp    *interp,
                          int            objc,
                          Tcl_Obj *const objv[] );

struct itkPointerTable
{
  static Tcl_Interp *interp;
  Tcl_HashTable PointerLookup;
  
  itkPointerTable( )
  {
    Tcl_InitHashTable( &( this->PointerLookup ), TCL_STRING_KEYS );
  }
  
  ~itkPointerTable( )
  {
    Tcl_DeleteHashTable( &( this->PointerLookup ) );
  }
  
  Tcl_Command GetPointerValue( itk::LightObject  *ptrObj )
  {
    char key[80];
    Tcl_HashEntry *entry;
    Tcl_Command token = NULL;

    sprintf( key, "%p", ptrObj );
    entry = Tcl_FindHashEntry( &( this->PointerLookup ), key );
    if ( entry ) {
      token = static_cast< Tcl_Command >Tcl_GetHashValue( entry );
    }
    return token;
  }
  
  void InsertPointer( itk::LightObject *ptrObj,
                      Tcl_Command       token )
  {
    char key[80];
    int is_new;
    Tcl_HashEntry *entry;

    sprintf( key, "%p", ptrObj );
    entry = Tcl_CreateHashEntry( &( this->PointerLookup ), key, &is_new );
    Tcl_SetHashValue( entry, static_cast< ClientData >( token ) );
  }

  int RemovePointer( itk::LightObject *ptrObj )
  {
    char key[80];
    Tcl_HashEntry *entry;

    sprintf( key, "%p", ptrObj );
    entry = Tcl_FindHashEntry( &( this->PointerLookup ), key );
    if ( entry ) {
      Tcl_DeleteHashEntry( entry );
      return TCL_OK;
    }
    return TCL_ERROR;
  }
};

Tcl_Interp *itkPointerTable::interp = NULL;

itkPointerTable *ITKTCL_GetPointerTable( Tcl_Interp *interp )
{
  ClientData table;

  table = Tcl_GetAssocData( interp, "itkassoc", NULL );
  return static_cast< itkPointerTable* >( table );
}

void ITKTCL_InterpDeleteProc( ClientData clientData, Tcl_Interp *interp )
{
  itkPointerTable* table = static_cast< itkPointerTable* >( clientData );

  delete table;
}

itkPointerTable *ITKTCL_InitPointerTable( Tcl_Interp *interp )
{
  itkPointerTable::interp = interp;
  
  itkPointerTable* table = new itkPointerTable( );
  Tcl_SetAssocData( interp, "itkassoc",
                    ITKTCL_InterpDeleteProc,
                    static_cast< ClientData >( table ) );
  return table;
}

void ITKTCL_AssocObject2Token( Tcl_Interp        *interp,
                               itk::LightObject  *itkObj,
                               Tcl_Command        token )
{
  itkPointerTable *table = ITKTCL_GetPointerTable( interp );
  table->InsertPointer( itkObj, token );
}

Tcl_Command ITKTCL_GetObjectAssoc( Tcl_Interp        *interp,
                                   itk::LightObject  *itkObj )
{
  itkPointerTable *table = ITKTCL_GetPointerTable( interp );
  Tcl_Command token = table->GetPointerValue( itkObj );
  return token;
}

void ITKTCL_DeleteObjectAssoc( itk::LightObject * itkObj )
{
  itkPointerTable *table = ITKTCL_GetPointerTable( itkPointerTable::interp );
  table->RemovePointer( itkObj );
}

struct itkObjectCommandData
{
  Tcl_ObjCmdProc *objProc;
  itk::LightObject::Pointer ptrObj;
  
  itkObjectCommandData( Tcl_ObjCmdProc *objProc, itk::LightObject *ptrObj )
  {
    this->objProc = objProc;
    /* automatic reference counting incremented */
    this->ptrObj = ptrObj;
  }
  
  ~itkObjectCommandData( )
  {
    /* automatic reference counting decremented */
    this->ptrObj = NULL;
  }

  void AssocObject2Token( Tcl_Interp *interp, Tcl_Command token )
  {
    ITKTCL_AssocObject2Token( interp, this->ptrObj, token );
  }
};

itk::LightObject*
ITKTCL_GetLightObjectFromCommandName( Tcl_Interp *interp,
                                      const char *cmdName )
{
  Tcl_CmdInfo info;
  int found;
  
  found = Tcl_GetCommandInfo( interp, cmdName, &info );
  if ( !found ) {
    return NULL;
  }
  // all ITKTCL commands share the same command wrapper which contain
  // the specific Tcl_ObjCmdProc attending the command
  if ( info.objProc != ITKTCL_ObjectCommand ) {
    Tcl_AppendResult( interp,
                      cmdName,
                      " is not a valid ITK object command",
                      NULL );
    return NULL;
  }
  itkObjectCommandData *cmdData =
    static_cast< itkObjectCommandData* >( info.objClientData );
  return cmdData->ptrObj;
};

int ITKTCL_ObjectCommand( ClientData     clientData,
                          Tcl_Interp    *interp,
                          int            objc,
                          Tcl_Obj *const objv[] )
{
  /* get access to the itk object command data */
  itkObjectCommandData *cmdData =
    static_cast<itkObjectCommandData*>( clientData );
  itk::LightObject *ptrObj = cmdData->ptrObj;

  /* deal with the subcommands common to every itktcl object */
  if ( objc < 4  ) {
    const char* subcmd = Tcl_GetString( objv[ 1 ] );
    if ( !strcmp( subcmd, "GetReferenceCount" ) ) {
      Tcl_SetObjResult( interp, Tcl_NewIntObj( ptrObj->GetReferenceCount( ) ) );
      return TCL_OK;
    } else if ( !strcmp( subcmd, "SetReferenceCount" ) ) {
      if ( objc != 3 ) {
        Tcl_AppendResult( interp, "wrong # of arguments, should be",
                          Tcl_GetString( objv[ 0 ] ), " ",
                          "SetReferenceCount RefCount",
                          NULL );
        return TCL_ERROR;
      }
      int refCount;

      if ( ITKTCL_GetPositiveIntegerNumber( interp, objv[2], &refCount ) == TCL_OK ) {
        ptrObj->SetReferenceCount( refCount );
        return TCL_OK;
      }
      Tcl_AppendResult( interp, "while invoking ",
                        Tcl_GetString( objv[ 0 ] ), " ",
                        "SetReferenceCount",
                        NULL );
                        
      return TCL_ERROR;
    }
  }
  /* invoke the actual command handler, the command receive as
     clientData the itkLightObject */
  return ( cmdData->objProc )( static_cast<ClientData>( ptrObj ),
                               interp, objc, objv );
}

void ITKTCL_DeleteObjectCommand( ClientData clientData )
{
  itkObjectCommandData *cmdData =
    static_cast< itkObjectCommandData* >( clientData );
  /* remove the association between the itk::LightObject and the command */
  ITKTCL_DeleteObjectAssoc( cmdData->ptrObj );
  /* remove the cmdData and UnRegister the itkLightObject contained in it */
  delete cmdData;
}

int ITKTCL_CreateObjectCommand( Tcl_Interp       *interp,
                                Tcl_ObjCmdProc   *cmdProc,
                                itk::LightObject *itkObj )
{
  static int icmd = 0;

  if ( !itkObj ) {
    Tcl_AppendResult( interp,
                      "could not create ITKCmd from NULL ITK object",
                      NULL );
    return TCL_ERROR;
  }
  char buffer[1024];
  sprintf( buffer, "itkCmd%i", icmd++ );
  itkObjectCommandData *cmdData = new itkObjectCommandData( cmdProc, itkObj );
  Tcl_Command token =
    Tcl_CreateObjCommand( interp, buffer,
                          ITKTCL_ObjectCommand,
                          static_cast< ClientData >( cmdData ),
                          ITKTCL_DeleteObjectCommand );
  if ( token ) {
    ITKTCL_AssocObject2Token( interp, itkObj, token );
    Tcl_SetResult( interp, buffer, TCL_VOLATILE );
    return TCL_OK;
  } else {
    Tcl_AppendResult( interp,
                      "fatal error: failed Tcl_CreateObjCommand",
                      NULL );
    return TCL_ERROR;
  }
}

int ITKTCL_GetCommand( Tcl_Interp       *interp,
                       Tcl_ObjCmdProc   *cmdProc,
                       itk::LightObject *itkObj )
{
  itkPointerTable *table = ITKTCL_GetPointerTable( interp );

  Tcl_Command token = table->GetPointerValue( itkObj );
  if ( token ) {
    char* cmdName = const_cast<char*>(Tcl_GetCommandName( interp, token ));
    Tcl_SetResult( interp, cmdName, TCL_VOLATILE );
    return TCL_OK;
  } else {
    return ITKTCL_CreateObjectCommand( interp, cmdProc, itkObj );
  }
}

int ITKTCL_Utils_Init( Tcl_Interp *interp )
{
  ITKTCL_InitPointerTable( interp );

  return TCL_OK;
}

int ITKTCL_GetNaturalNumber( Tcl_Interp *interp,
                             Tcl_Obj* obj, int* ptrValue )
{
  if ( Tcl_GetIntFromObj(interp, obj, ptrValue) != TCL_OK ) {
    // no se ha podido convertir a entero
    return TCL_ERROR;
  }
  if ( *ptrValue <= 0 ) {
    Tcl_AppendResult( interp, "invalid value '", Tcl_GetString( obj ),
                      "' ", "must be a natural number", NULL );
    return TCL_ERROR;
  }
  return TCL_OK;
}

int ITKTCL_GetPositiveIntegerNumber( Tcl_Interp *interp,
                                     Tcl_Obj* obj, int* ptrValue )
{
  if ( Tcl_GetIntFromObj(interp, obj, ptrValue) != TCL_OK ) {
    // no se ha podido convertir a entero
    return TCL_ERROR;
  }
  if ( *ptrValue < 0 ) {
    Tcl_AppendResult( interp, "invalid value '", Tcl_GetString( obj ),
                      "' ", "must be a positive integer.", NULL );
    return TCL_ERROR;
  }
  return TCL_OK;
}

int ITKTCL_GetDouble( Tcl_Interp *interp,
                                     Tcl_Obj* obj, double* ptrValue )
{
  if ( Tcl_GetDoubleFromObj(interp, obj, ptrValue) != TCL_OK ) {
    // no se ha podido convertir a entero
    Tcl_AppendResult( interp, "invalid value '", Tcl_GetString( obj ),
                      "' ", "must be a valid double value ", NULL );
    return TCL_ERROR;
  }
  return TCL_OK;
}
