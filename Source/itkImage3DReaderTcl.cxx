#include "itkUtilsTcl.h"
#include "itkImageTypes.h"
#include "itkImageTcl.h"
#include "itkImageFileReader.h"

typedef itk::ImageFileReader< Scalar3DImageType > Reader3DType;

int Image3DReaderCmd( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[] )
{
  // this is how we get the pointer to the itk::Object
  itk::LightObject *itkObj =
    static_cast< itk::LightObject* >( clientData );
  Reader3DType::Pointer reader = dynamic_cast< Reader3DType* >( itkObj );

  if ( !reader ) {
      Tcl_AppendResult( interp,
                        "command ", Tcl_GetString( objv[ 0 ] ),
                        " does not references a valid reader object",
                        NULL );
      return TCL_ERROR;
  }
  if ( objc < 2 ) {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    Tcl_AppendResult( interp,
                      "Image3DReader ERROR\n",
                      "wrong # of arguments, must be:\n",
                      "    ", cmd, " Update\n"
                      "    ", cmd, " SetFileName path_to_image\n",
                      "    ", cmd, " GetOutput",
                      NULL );
      return TCL_ERROR;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  // Update
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "Update" ) ) 
  {
    try {
      reader->Update( );
    } catch ( itk::ExceptionObject e ) {
      Tcl_AppendResult( interp, e.GetDescription(), NULL );
      return TCL_ERROR;
    }
    return TCL_OK;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  // SetFileName
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  else if( !strcmp( Tcl_GetString( objv[ 1 ] ), "SetFileName" ) ) 
  {
    if ( objc != 3) {
      const char* cmd = Tcl_GetString( objv[ 0 ] );
      Tcl_AppendResult( interp,
                        "wrong # of arguments, must be:\n",
                        "    ", cmd, " SetFileName path_to_image",
                        NULL );
      return TCL_ERROR;
    }
    reader->SetFileName( Tcl_GetString( objv[ 2 ] ) );
    return TCL_OK;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  // GetOutput
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetOutput" ) ) 
  {
    return ITKTCL_GetOutput<Scalar3DImageType>( interp,  reader->GetOutput( ) );
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  // Else
  //////////////////////////////////////////////////////////////////////////////////////////////////////
  else 
  {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    Tcl_AppendResult( interp,
                      "Image3DReader ERROR\n",
                      "wrong subcommand ",
                      Tcl_GetString( objv[ 1 ] ),
                      " must be:\n",
                      "    ", cmd, " Update\n"
                      "    ", cmd, " SetFileName path_to_image\n",
                      "    ", cmd, " GetOutput",
                      NULL );
    return TCL_ERROR;
  }
}

int CreateImage3DReader( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[] )
{
  Reader3DType::Pointer reader = Reader3DType::New();
  return ITKTCL_CreateObjectCommand( interp, Image3DReaderCmd, reader );
}

int ITKTCL_Image3DReader_Init( Tcl_Interp *interp )
{
  Tcl_CreateObjCommand( interp, "Image3DReader", CreateImage3DReader,
                        NULL, NULL );
  return TCL_OK;
}
