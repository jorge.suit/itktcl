#ifndef _ITK_IMAGE_TCL_H_
#define _ITK_IMAGE_TCL_H_

#include <tcl.h>
#include "itkUtilsTcl.h"
#include "itkImageTypes.h"

int Tcl_SetPixelValueResult( Tcl_Interp *interp, const VPixel3Type & pixel );
int Tcl_SetPixelValueResult( Tcl_Interp *interp, const ConvariantVector3 & pixel );
int Tcl_SetPixelValueResult( Tcl_Interp *interp, PixelType pixel );
int Tcl_SetPixelValueResult( Tcl_Interp *interp, MaskPixelType pixel );
int Tcl_SetPixelValueResult( Tcl_Interp *interp, int pixel );

int Tcl_GetPixelValueFromObj( Tcl_Interp *interp, Tcl_Obj * obj,
                              PixelType &pixelValue );
int Tcl_GetPixelValueFromObj( Tcl_Interp *interp, Tcl_Obj * obj,
                              MaskPixelType &pixelValue );

int Tcl_SetPixelSizeResult( Tcl_Interp *interp, const VPixel3Type & pixel );
int Tcl_SetPixelSizeResult( Tcl_Interp *interp, const ConvariantVector3 & pixel );
int Tcl_SetPixelSizeResult( Tcl_Interp *interp, PixelType pixel );

int ITKTCL_Image_Init( Tcl_Interp *interp );

template <typename TImage>
int ImageCmd( TImage *ptrImg, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[] ){
  if ( !ptrImg ) {
    Tcl_AppendResult( interp,
                      "the command ", Tcl_GetString( objv[ 0 ] ),
                      " is corrupted: could not obtain a valid ImageType",
                      NULL );
  }
  if ( objc == 1 ) {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    Tcl_AppendResult( interp,
                      "wrong # of arguments, must be:\n",
                      "    ", cmd, " GetNameOfClass\n",
                      "    ", cmd, " GetImageDimension\n",
                      "    ", cmd, " GetOrigin\n",
		      "    ", cmd, " InitializeOrigin\n",
		      "    ", cmd, " GetSize\n",
                      "    ", cmd, " GetSpacing\n",
                      "    ", cmd, " GetPixelValue\n",
                      "    ", cmd, " GetPixelSize\n",
                      "    ", cmd, " DisconnectPipeline\n",
                      "    ", cmd, " Print\n",
                      "    ", cmd, " Update",
                      NULL );
    return TCL_ERROR;
  }
  //////////////////////////////////////////////////////////////////////////////
  // GetNameOfClass
  //////////////////////////////////////////////////////////////////////////////
  unsigned int dim = ptrImg->GetImageDimension( );
  if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetNameOfClass" ) ) {
    const char *name = ptrImg->GetNameOfClass( );
    Tcl_SetResult( interp, const_cast<char*>(name), TCL_VOLATILE );
  }

  //////////////////////////////////////////////////////////////////////////////
  // GetImageDimension
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetImageDimension" ) ) {
    Tcl_Obj * result = Tcl_NewIntObj( dim );
    Tcl_SetObjResult( interp, result );
    return TCL_OK;
  }

  //////////////////////////////////////////////////////////////////////////////
  // GetOrigin
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetOrigin" ) ) 
  {
    typename TImage::PointType origin = ptrImg->GetOrigin( );
    Tcl_Obj **comp = new Tcl_Obj*[ dim ];
    for ( unsigned int i = 0; i < dim; i++ ) {
      comp[i] = Tcl_NewDoubleObj( origin[ i ] );
    }
    Tcl_Obj * result = Tcl_NewListObj( dim, comp );
    delete[] comp;
    Tcl_SetObjResult( interp, result );
    return TCL_OK;
  }
  //////////////////////////////////////////////////////////////////////////////
  // SetOrigin
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "InitializeOrigin" ) ) 
  {
    typename TImage::PointType origin ;
    for ( unsigned int i = 0; i < dim; i++ ) {
      origin[i] = 0;
    }
    ptrImg->SetOrigin( origin );
    return TCL_OK;
  }
  
  //////////////////////////////////////////////////////////////////////////////
  // GetSize
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetSize" ) ) 
  {
    typename TImage::RegionType region = ptrImg->GetLargestPossibleRegion();
    typename TImage::SizeType size = region.GetSize();

    Tcl_Obj **comp = new Tcl_Obj*[ dim ];
    for ( unsigned int i = 0; i < dim; i++ ) {
      comp[i] = Tcl_NewIntObj( size[ i ] );
    }
    Tcl_Obj * result = Tcl_NewListObj( dim, comp );
    delete[] comp;
    Tcl_SetObjResult( interp, result );
    return TCL_OK;
  }
  //////////////////////////////////////////////////////////////////////////////
  // GetSpacing
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetSpacing" ) ) 
  {
    typename TImage::SpacingType spacing = ptrImg->GetSpacing( );
    Tcl_Obj **comp = new Tcl_Obj*[ dim ];
    for ( unsigned int i = 0; i < dim; i++ ) {
      comp[i] = Tcl_NewDoubleObj( spacing[ i ] );
    }
    Tcl_Obj * result = Tcl_NewListObj( dim, comp );
    delete[] comp;
    Tcl_SetObjResult( interp, result );
    return TCL_OK;
  }
  //////////////////////////////////////////////////////////////////////////////
  // GetPixelValue
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetPixelValue" ) ) 
  {
    if ( objc != 3 ) {
      const char* cmd = Tcl_GetString( objv[ 0 ] );
      Tcl_AppendResult( interp,
                        "wrong # of arguments, must be:\n",
                        "    ", cmd, " GetPixelValue list_of_pixel_coord\n",
                        NULL );
      return TCL_ERROR;
    }
    Tcl_Obj **pixel_objv;
    int pixel_objc;
    typename TImage::IndexType pixelIndex;
    if ( Tcl_ListObjGetElements(interp, objv[ 2 ], &pixel_objc, &pixel_objv ) != TCL_OK ) {
      return TCL_ERROR;
    }
    if ( static_cast<unsigned int>(pixel_objc) != dim ) {
      Tcl_AppendResult( interp,
                        "invalid pixel index must be a list of ", dim, " integers",
                        NULL );
      return TCL_ERROR;
    }
    for ( unsigned int i = 0; i < dim; i++ ) {
      int idx;
      if ( Tcl_GetIntFromObj( interp, pixel_objv[i], &idx ) != TCL_OK ) {
        return TCL_ERROR;
      }
      pixelIndex[ i ] = idx;
    }
    typename TImage::PixelType pixelValue = ptrImg->GetPixel( pixelIndex );
    // here we must deal with vector pixel value
    return Tcl_SetPixelValueResult( interp, pixelValue );
  }
  //////////////////////////////////////////////////////////////////////////////
  // GetPixelSize
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetPixelSize" ) )
  {
    // Declare a pixel (the first one) and get its size 
    typename TImage::IndexType pixelIndex;
    for ( unsigned int i = 0; i < dim; i++ ) {
      pixelIndex[ i ] = 0;
    }
    assert( ptrImg != NULL );
    typename TImage::PixelType pixelValue = ptrImg->GetPixel( pixelIndex );

    return Tcl_SetPixelSizeResult( interp, pixelValue );

  }
  //////////////////////////////////////////////////////////////////////////////
  // DisconnectPipeline
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "DisconnectPipeline" ) )
  {
    ptrImg->DisconnectPipeline();
    return TCL_OK;
  }
  //////////////////////////////////////////////////////////////////////////////
  // Print
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "Print" ) ) 
    {
      ptrImg->Print( std::cout );
      return TCL_OK;
    }
  //////////////////////////////////////////////////////////////////////////////
  // Update
  //////////////////////////////////////////////////////////////////////////////
  else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "Update" ) ) 
  {
    try {
      ptrImg->Update( );
    } catch ( itk::ExceptionObject e ) {
      Tcl_AppendResult( interp, e.GetDescription(), NULL );
      return TCL_ERROR;
    }
    return TCL_OK;
  } else if ( !strcmp( Tcl_GetString( objv[ 1 ] ), "GetReferenceCount" ) ) {
    Tcl_Obj * result = Tcl_NewIntObj( ptrImg->GetReferenceCount( ) );
    Tcl_SetObjResult( interp, result );
    return TCL_OK;
  } else {
    const char* cmd = Tcl_GetString( objv[ 0 ] );
    Tcl_AppendResult( interp,
                      "Invalid argument '", 
                      Tcl_GetString( objv[ 1 ] ), "', must be:\n",
                      "    ", cmd, " GetNameOfClass\n",
                      "    ", cmd, " GetImageDimension\n",
                      "    ", cmd, " GetOrigin\n",
		      "    ", cmd, " InitializeOrigin\n",
		      "    ", cmd, " GetSize\n", 
                      "    ", cmd, " GetSpacing\n",
                      "    ", cmd, " GetPixelValue\n",
                      "    ", cmd, " GetPixelSize\n",
                      "    ", cmd, " DisconnectPipeline\n",
                      "    ", cmd, " Print\n",
                      "    ", cmd, " Update",
                      NULL );
    return TCL_ERROR;
  }
  return TCL_OK;
}

template < typename TImage >
int ImageCommand( ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *const objv[] )
{
  // this is how we get the pointer to the itk::Object
  itk::LightObject *itkObj =
    reinterpret_cast< itk::LightObject* >( clientData );

  TImage *ptrImg = dynamic_cast< TImage* >( itkObj );
  if ( !ptrImg ) {
    Tcl_AppendResult( interp,
                      "could not create and output Image from an invalid Image Type",
                      NULL );
    return TCL_ERROR;
  }
  ImageCmd<TImage>( ptrImg, interp, objc, objv );
  return TCL_OK;
}

template < typename TImage >
int ITKTCL_GetOutput( Tcl_Interp *interp, itk::LightObject *itkObj )
{
  if( !itkObj ) {
    return TCL_ERROR;
  }

  TImage *ptrImg = dynamic_cast< TImage* >( itkObj );
  if ( !ptrImg ) {
    Tcl_AppendResult( interp,
                      "could not create and output Image from an invalid ImageType",
                      NULL );
    return TCL_ERROR;
  }
  // check if that ptrImg has a command associated 
  return ITKTCL_GetCommand( interp, ImageCommand<TImage>, ptrImg );
}

template < typename TImage >
int ITKTCL_GetInput( Tcl_Interp *interp, itk::LightObject *itkObj )
{
  if( !itkObj ) {
    return TCL_ERROR;
  }

  TImage *ptrImg = dynamic_cast< TImage* >( itkObj );
  if ( !ptrImg ) {
    Tcl_AppendResult( interp,
                      "could not create and input Image from an invalid ImageType",
                      NULL );
    return TCL_ERROR;
  }
  // check if that ptrImg has a command associated 
  return ITKTCL_GetCommand( interp, ImageCommand<TImage>, ptrImg );
}

template <class TImageType>
TImageType *ITKTCL_GetImageFromCommand( Tcl_Interp *interp,
                                         const char* ImageCmd )
{
  itk::LightObject *ptrObj = ITKTCL_GetLightObjectFromCommandName( interp, ImageCmd );
  TImageType *ptrImg = dynamic_cast<TImageType *>( ptrObj );
  return ptrImg;
}

template <class TFilterType, class TInputImageType>
int ITKTCL_SetInput( Tcl_Interp *interp, TFilterType *Filter, const char* ImageCmd )
{
  itk::LightObject *ptrObj = ITKTCL_GetLightObjectFromCommandName( interp, ImageCmd );
  
  TInputImageType *ptrImg = dynamic_cast<TInputImageType *>( ptrObj );
  if ( *ImageCmd && !ptrImg ) {
    Tcl_AppendResult( interp, ImageCmd, " is not a valid ImageType",  NULL );
    return TCL_ERROR;
  }
  Filter->SetInput( ptrImg );
  return TCL_OK;
}

template <class TFilterType, class TInputImageType>
int ITKTCL_SetInput( Tcl_Interp *interp, TFilterType *Filter, int n, const char* ImageCmd )
{
  itk::LightObject *ptrObj = ITKTCL_GetLightObjectFromCommandName( interp, ImageCmd );
  TInputImageType *ptrImg = dynamic_cast<TInputImageType *>( ptrObj );
  if ( *ImageCmd && !ptrImg ) {
    Tcl_AppendResult( interp, ImageCmd, " is not a valid ImageType",  NULL );
    return TCL_ERROR;
  }
  Filter->SetInput(n, ptrImg );
  return TCL_OK;
}

template <class TFilterType, class TInputImageType>
int ITKTCL_SetInput1( Tcl_Interp *interp, TFilterType *Filter, const char* ImageCmd )
{
  itk::LightObject *ptrObj = ITKTCL_GetLightObjectFromCommandName( interp, ImageCmd );
  TInputImageType *ptrImg = dynamic_cast<TInputImageType *>( ptrObj );
  if ( !ptrImg ) {
    Tcl_AppendResult( interp, ImageCmd, " is not a valid ImageType",  NULL );
    return TCL_ERROR;
  }
  Filter->SetInput1( ptrImg );
  return TCL_OK;
}

template <class TFilterType, class TInputImageType>
int ITKTCL_SetInput2( Tcl_Interp *interp, TFilterType *Filter, const char* ImageCmd )
{
  itk::LightObject *ptrObj = ITKTCL_GetLightObjectFromCommandName( interp, ImageCmd );
  TInputImageType *ptrImg = dynamic_cast<TInputImageType *>( ptrObj );
  if ( !ptrImg ) {
    Tcl_AppendResult( interp, ImageCmd, " is not a valid ImageType",  NULL );
    return TCL_ERROR;
  }
  Filter->SetInput2( ptrImg );
  return TCL_OK;
}

template <class TFilterType, class TInputImageType>
int ITKTCL_SetFeatureImage( Tcl_Interp *interp, TFilterType *Filter, const char* ImageCmd )
{
  itk::LightObject *ptrObj = ITKTCL_GetLightObjectFromCommandName( interp, ImageCmd );
  TInputImageType *ptrImg = dynamic_cast<TInputImageType *>( ptrObj );
  if ( !ptrImg ) {
    Tcl_AppendResult( interp, ImageCmd, " is not a valid ImageType",  NULL );
    return TCL_ERROR;
  }
  Filter->SetFeatureImage( ptrImg );
  return TCL_OK;
}

#endif
